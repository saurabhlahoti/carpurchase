package com.assignment2.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

import com.assignment2.entities.*;
import com.assignment2.util.Choice;
import com.assignment2.util.carBrand;

public class Controller {
	static BufferedReader reader = new BufferedReader(new InputStreamReader(
			System.in));
	static HashMap<Integer, Customer> custHash = new HashMap<Integer, Customer>(); // static

	public static void main(String[] args) throws java.lang.Exception {

		int choiceInt = 0;
		while (true) {
			System.out.println(" 1. Enter Customer Details" + " \n 2. Add car "
					+ "\n 3. Get by ID" + "\n 4. Sort by Name "
					+ "\n 5. Generate Prizes" + "\n 6. Exit");
			choiceInt = Integer.parseInt(reader.readLine());
			choiceInt--;
			try {
				Choice ch = Choice.values()[choiceInt];
				switch (ch) {
				case ADD_CUSTOMER:
					addCustomer();
					break;
				case ADD_CAR:
					addCar();
					break;
				case GET_BY_ID:
					getById();
					break;
				case SORT_BY_NAME:
					sortByName();
					break;
				case PRIZES:
					givePrizes();
					break;
				default:
					return;
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				return;

			}

		}

	}

	static Car carDetail() {
		int carId = 1;
		String model = "BMW";
		float price = 0.0f;
		System.out.println("Enter carID :");
		Car car = new Car();
		try {
			carId = Integer.parseInt(reader.readLine());
			System.out.print(" 1. Toyota " + "\n 2. Maruti" + "\n 3. Hyundai ");
			int carChoice = Integer.parseInt(reader.readLine());
			carChoice--;
			carBrand ch = carBrand.values()[carChoice];
			System.out.println("Enter model name :");
			model = reader.readLine();
			System.out.println("Enter price :");
			price = Float.parseFloat(reader.readLine());

			switch (ch) {
			case TOYOTA:
				Toyota toyota = new Toyota(model);
				toyota.setValues(carId, price);
				return toyota;
			case MARUTI:
				Maruti maruti = new Maruti(model);
				maruti.setValues(carId, price);
				return maruti;
			case HYUNDAI:
				Hyundai hyundai = new Hyundai(model);
				hyundai.setValues(carId, price);
				return hyundai;
			}

		} catch (java.lang.Exception e) {
			e.getMessage();
		}

		return car;
	}

	static void addCustomer() { // function to add new customers
		int id;
		String name;
		ArrayList<Car> carList = new ArrayList<Car>();
		try {
			System.out.print("\nEnter Customer Id :");
			id = Integer.parseInt(reader.readLine());
			while (custHash.containsKey(id)) {
				System.out
						.print("\nID Already Exists. Enter different id OR enter 0 to exit to main menu : ");
				id = Integer.parseInt(reader.readLine());
				if (id == 0)
					break;
			}
			if (id != 0) {
				System.out.println("Enter customers name :");
				name = reader.readLine();
				carList.add(carDetail());
				Customer customer = new Customer(id, name, carList);
				custHash.put(id, customer);
			}
		} catch (java.lang.Exception e) {
			e.getMessage();
		}
	}

	static void addCar() { // function to add a car to existing customer
		int id;
		System.out.print("Enter customer ID : ");
		try {
			id = Integer.parseInt(reader.readLine());
			while (!custHash.containsKey(id)) {
				System.out
						.print("\nID Doesnot Exist. Enter different id OR enter 0 to exit to main menu : ");
				id = Integer.parseInt(reader.readLine());
				if (id == 0)
					break;
			}
			Customer customer = custHash.get(id);
			customer.car.add(carDetail());
			custHash.put(id, customer);
		} catch (java.lang.Exception e) {
			e.getMessage();
		}
	}

	static void getById() { // get the customer details using his ID
		int id;
		System.out.print("Enter customer ID : ");
		try {
			id = Integer.parseInt(reader.readLine());
			if (custHash.containsKey(id)) {
				Customer customer = custHash.get(id);
				System.out.println(customer);
				for (int i = 0; i < customer.car.size(); i++) {
					Car car = customer.car.get(i);
					System.out.println(car);
				}
			} else {
				System.out.println("Customer ID doesn't exist..\n");
			}
		} catch (java.lang.Exception e) {
			e.getMessage();
		}
	}

	private static Comparator<Customer> byName() { // making a customized
													// comparator
		return new Comparator<Customer>() {
			// @Override
			public int compare(Customer c1, Customer c2) {
				return c1.name.compareTo(c2.name);
			}
		};
	}

	static void sortByName() { // function to sort by name
		ArrayList<Customer> customList = new ArrayList<Customer>(
				custHash.values());
		Collections.sort(customList, byName());
		for (int i = 0; i < customList.size(); i++) {
			System.out.println(customList.get(i));
		}
	}

	static void givePrizes() { // function to give away prizes to random IDs
		int random = 6;
		ArrayList<Integer> keysArray = new ArrayList<Integer>(custHash.keySet());
		int[] userKeys = new int[3];
		HashMap<Integer, Customer> randomKeys = new HashMap<Integer, Customer>();
		Random r = new Random();
		while (random > 0) {
			int key = keysArray.get(r.nextInt(keysArray.size()));
			if (!randomKeys.containsKey(key)) {
				randomKeys.put(key, custHash.get(key));
				random--;
			}
		}
		System.out.print("Enter three keys :");
		for (int i = 0; i < 3; i++) {
			System.out.println("Enter key :" + (i + 1) + "\n");
			try {
				userKeys[i] = Integer.parseInt(reader.readLine());
			} catch (java.lang.Exception e) {
				e.getMessage();
			}
		}

		for (int i = 0; i < 3; i++) {
			if (randomKeys.containsKey(userKeys[i])) {
				System.out.println("\nLucky winner is :-");
				System.out.println(randomKeys.get(userKeys[i]));
			}
		}

	}
}
