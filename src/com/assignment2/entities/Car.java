package com.assignment2.entities;

public class Car {
	int id;
	String model;
	float price, resaleValue;

	public Car() {
	}

	public void setValues(int i, float p) {
		id = i;
		price = p;
		resaleValue *= price;
	}

	public String toString() {
		return "Car ID " + id + " \nModel : " + model + " \nPrice : " + price
				+ " \nResale Value :" + resaleValue;
	}

}
