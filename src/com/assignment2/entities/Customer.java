package com.assignment2.entities;

import java.util.*;

public class Customer {
	int id;
	public String name;
	public ArrayList<Car> car = new ArrayList<Car>();

	public Customer(int i, String n, ArrayList<Car> list) {
		id = i;
		name = n;
		car = list;
	}

	public String toString() {
		return "Customer ID " + id + " \nName : " + name;
	}
}
