package com.assignment2.entities;

public class Toyota extends Car {
	public Toyota(String s) {
		model = "Toyota " + s;
		resaleValue = 0.8f;
	}
}