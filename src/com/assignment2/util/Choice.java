package com.assignment2.util;

public enum Choice {
	ADD_CUSTOMER, ADD_CAR, GET_BY_ID, SORT_BY_NAME, PRIZES, EXIT;
}
